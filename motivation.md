Greetings from Balázs the Katona, and please just call me Katona,

  In this letter you shall learn briefly what is my experince with Coding,
and what kind of person I am.

  Firstly my latest experience comes from Cinemo a German automotive company, where I 
worked for a year from Budapest, purely remote (Home Office only) where my position was 
(Quality) Software Engineer, with tasks requiring C/C++.
Furthermore, with a freshly opened office our team of three devs and a leader had a task
to create an automatic testing environment for a codebase aged at least 10 years wrapped with
moderate documentation and obsolate technologies. However fortunately we had the upper hand
choosing the tools for this task, therefore we chose [RobotFramework](https://robotframework.org/) utilizing [CYthon](https://cython.org/) implementation and [swig headers](http://www.swig.org/)
 for the C/C++ library. During this time I have used mainly C++17 [with backward compatible interfaces] and learned
docker, python(python3.{6,7,8,9};[tox](https://pypi.org/project/tox/);[pytest](https://docs.pytest.org/en/6.2.x/contents.html)} and RobotFramework in Linux enviroment
[Arch for my working machine and Ubuntu as docker image] occasionally configuring Jenkins jobs
using native git and Bitbucket.

  So who I am? I am a 27-years-old young man who has been Coding in C++ for four years. 
I left the university [ELTE-IK Computer Science] due to full-time C++ Developer (Qt5) opportunity back in 2017, before that I have been an intern for a year [JAVA]. If you would hire me you could expect a hardworking, trustworthy, honest and straightforward man, whom is ready to extend his knowledge armored with the Angel of Work and Joy which philosophy and duty can bring to a man. 
  

  Lastly few words about my hobbies including but not limited to reading books, looking for patterns in Old texts, hiking, biking and camping in Nature,
attending and helping out in friendly gardenparties, playing challenging PC games.
And of course Coding for fun. [gitlab repo](https://gitlab.com/KappaTona).


Let me share some of my favorite stuffs:

Music: [Let there be Light (Outsisders)](https://open.spotify.com/track/4hTwPhvVNgucjuXffwHGnx?si=19ab5e77954943a0), [Viken Arman Hope](https://soundcloud.com/vikenarman/hope)

Books: Marcus Aurelius Meditation, Book of Enoch I/II, 
[more](https://gitlab.com/KappaTona/aand/-/blob/journal-try/MISC/history.md#L954)

Films: [The Ten Commandments](https://www.rottentomatoes.com/m/1021015-ten_commandments), [Bruce Lee the way of the Dragon](https://www.imdb.com/title/tt0068935/)

Series: [Love Death Robots](https://www.imdb.com/title/tt9561862/?ref_=nv_sr_srsg_0), [Stargate SG-1](https://www.imdb.com/title/tt0118480/?ref_=nv_sr_srsg_0)

[Favorite Seneca Leterrs: 9, 20, 23, 66](https://www.youtube.com/watch?v=2-Kcs89WuWw)

PC Games: Dark Souls Remastered, Dark Souls III, Sekiro

Thanks to [WineHQ](https://www.winehq.org/) and [Steam Proton](https://www.protondb.com/) I could played
these games from Linux.

[Sekiro Boss(es) No Hit Challange](https://www.youtube.com/playlist?list=PL7OfFdbG7gk7DVUZGKKgWoOrE8SgSvPB2)

Strength, Peace and with good Courage fulfilled hearth shall be upon us.
Farewell and looking forward to hearing from you,

Katona
